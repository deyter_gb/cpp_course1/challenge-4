#include <iostream>
#include <fstream>
#include <filesystem>
#include "arrayHelpers.h"
#include "fileHelpers.h"
#include "library.h"

/* #region constants */
const std::string TASK_SEPARATOR = "========================================================";

const size_t MIN_LENGTH = 1;
const size_t MAX_LENGTH = 31;

const size_t MATRIX_SIZE = 4;
/* #endregion */

/* #region Helper functions */
/**
 * Print separator between tasks.
 */
void PrintSeparator() {
  std::cout << TASK_SEPARATOR << std::endl;
}

bool CheckForSubstring(const std::string *readed_string, const char *STRING_TO_FOUND) {
  if ((*readed_string).find(STRING_TO_FOUND) != std::string::npos) {
    std::cout << "Found" << std::endl;
    return true;
  } else {
    std::cout << "Not found" << std::endl;
    return false;
  }
}
/* #endregion */

/* #region Task functions */
void Task1() {
  size_t size = 0;
  std::cout << "Enter size of array from " << MIN_LENGTH << "to " << MAX_LENGTH << ":";
  while (size < MIN_LENGTH || size > MAX_LENGTH) {
    std::cin >> size;
  }
  int *i_d_arr = InitArrayWithPowOfTwo(size);
  PrintArray(i_d_arr, size);
  delete[] i_d_arr;
}

void Task2() {
  int **i_matrix = InitMatrixWithRand(MATRIX_SIZE);
  PrintMatrix(i_matrix, MATRIX_SIZE);
  DeleteMatrix(i_matrix, MATRIX_SIZE);
}

void Task3(std::string &filename1, std::string &filename2) {
  std::cout << "Enter first filename:";
  std::cin >> filename1;
  std::cout << "Enter second filename:";
  std::cin >> filename2;
  InitRandomCharsAndSaveToFile(filename1);
  InitRandomCharsAndSaveToFile(filename2);
}

void Task4(const std::string &filename1, const std::string &filename2) {
  std::string filename3;
  std::cout << "Enter third filename:";
  std::cin >> filename3;
  std::ofstream fout(filename3);
  size_t filesize1 = std::filesystem::file_size(filename1) / sizeof(char);
  size_t filesize2 = std::filesystem::file_size(filename2) / sizeof(char);
  char *input = new char[filesize1 + filesize2];
  int i = ReadFromFileAndGetDelta(filename1, filesize1, input, 0);
  ReadFromFileAndGetDelta(filename2, filesize1 + filesize2, input, i);
  PrintCharsToFile(filename3, input, filesize1 + filesize2);
  delete[] input;
}

void Task5() {
  std::string filename;
  std::cout << "Enter filename:";
  std::cin >> filename;
  const char CONTENT[] =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

  PrintCharsToFile(filename, CONTENT, sizeof(CONTENT));

  auto *readed_string = ReadStringFromFile(filename);

  const char *STRING_TO_FOUND = "labore";
  CheckForSubstring(readed_string, STRING_TO_FOUND);

  const char *STRING_TO_NOT_FOUND = "1234567";
  CheckForSubstring(readed_string, STRING_TO_NOT_FOUND);
  delete[] readed_string;
}
/* #endregion */
