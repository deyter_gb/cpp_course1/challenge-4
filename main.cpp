#include <iostream>
#include "library.h"

int main() {
  Task1();
  PrintSeparator();
  Task2();
  PrintSeparator();
  std::string filename1, filename2;
  Task3(filename1, filename2);
  PrintSeparator();
  Task4(filename1, filename2);
  PrintSeparator();
  Task5();
  return 0;
}
