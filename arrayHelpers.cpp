//
// Created by aard on 23.09.2021.
//
#include <iostream>

const size_t CHAR_MIN_SYMBOL = 33;
const size_t CHAR_SYMBOLS_COUNT = 93;

int *InitArrayWithPowOfTwo(const size_t SIZE) {
  int *i_d_arr = new int[SIZE];
  i_d_arr[0] = 1;
  for (size_t i = 1; i < SIZE; i++) {
    i_d_arr[i] = i_d_arr[i - 1] * 2;
  }
  return i_d_arr;
}

int **InitMatrixWithRand(const size_t SIZE) {
  int **i_matrix = new int *[SIZE];
  for (size_t i = 0; i < SIZE; i++) {
    i_matrix[i] = new int[SIZE];
  }
  for (int i = 0; i < SIZE; i++) {
    for (size_t j = 0; j < SIZE; j++) {
      i_matrix[i][j] = rand();
    }
  }
  return i_matrix;
}

char *InitArrayWithChars(const size_t SIZE) {
  char *arr = new char[SIZE];
  for (size_t i = 0; i < SIZE; i++) {
    arr[i] = char(CHAR_MIN_SYMBOL + (rand() % CHAR_SYMBOLS_COUNT));
  }
  return arr;
}

bool PrintArray(int arr[], size_t size) {
  if (arr == nullptr || size == 0) {
    return false;
  }
  std::cout << "Array: " << std::endl;
  for (size_t i = 0; i < size; i++) {
    std::cout << "Index:" << i << " Value:" << arr[i] << std::endl;
  }
  std::cout << std::endl;
  return true;
}

bool PrintMatrix(int **arr, size_t size) {
  if (arr == nullptr || size == 0) {
    return false;
  }
  std::cout << "Matrix: " << std::endl;
  for (size_t i = 0; i < size; i++) {
    for (size_t j = 0; j < size; j++) {
      std::cout << arr[i][j] << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
  return true;
}

void DeleteMatrix(int **arr, size_t size) {
  for (size_t i = 0; i < size; i++) {
    delete[] arr[i];
  }
  delete[] arr;
}
