#ifndef CHALLENGE4_LIBRARY_H
#define CHALLENGE4_LIBRARY_H

void Task1();
void Task2();
void Task3(std::string &filename1, std::string &filename2);
void Task4(const std::string &filename1, const std::string &filename2);
void Task5();
void PrintSeparator();

#endif //CHALLENGE4_LIBRARY_H
